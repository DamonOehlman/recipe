# recipe

This library provides a set of Javascript helpers for interpreting textual recipes which define the requirements of a web resource.  Recipe is not designed to function as a build-tool in it's own right, this is left to tools such as [bake-js](/DamonOehlman/bake-js) and others.

<a href="http://travis-ci.org/#!/buildjs/recipe"><img src="https://secure.travis-ci.org/buildjs/recipe.png" alt="Build Status"></a>

Any standalone that can be accessed via HTTP can be described in a compo recipe.  For example, the following is a recipe that describes [keymaster](/madrobby/keymaster) using a [getit custom url scheme](/DamonOehlman/getit#custom-url-schemes):

```
# dsc: A simple micro-library for defining and dispatching keyboard shortcuts.
# url: https://github.com/madrobby/keymaster

[core]
js <= github://madrobby/keymaster/keymaster.js
```

## What about Web Components?

There is a lot of talk going on around [Web Components](http://dvcs.w3.org/hg/webcomponents/raw-file/tip/explainer/index.html) at the moment, but this project isn't designed with Web Components in mind and so the terminology has been avoided.

What recipe is though is a:

- constistent, reusable way of definining reusable resources on the web

While we are avoiding term component, many of the principles of component design apply if you are looking to create useful, re-usable web resources.  In particular good, re-usable web resources should be:

- __FOCUSED__, which usually means __SMALL__.  Have a well defined purpose and scope, and generally small defined surface area makes a component both easy to understand and also easy to test.  This should be encouraged.

- __COMPOSABLE__.  The ability to take the component and use it in ways not envisaged (but still "correct") by the component author demonstrates good component design.