
var formatter = require('formatter');


var reDownload = /^([\w\.\-]*?)\s*<\=\s*(.*)$/,
    reCommaDelim = /\,\s*/,
    reField = /^\s*\#?\s*(\w+)\:\s*(.*)$/,
    reLocalFile = /^[\/\\\.]/,
    reSection = /\s*\[(\w+)\]/,
    reComplexVariant = /^(\w+)\s*\[(.*)\]\s*$/;
function parseRequirements(text) {
	var requirements = text.split(reCommaDelim);

	// iterate through the requirements and update to absolute paths if it they are local
	requirements.forEach(function(req, index) {
	    if (reLocalFile.test(req)) {
	        requirements[index] = path.resolve(recipe.basePath, req);
	    }
	});

	return requirements;
}

function parseVersions(value) {
    if (typeof value == 'string' || (value instanceof String)) {
        value = value.split(reCommaDelim);

        // if we don't have a latest version specified, then add that to the list
        if (value.indexOf('latest') < 0) {
            value[value.length] = 'latest';
        }
    }

    return value;
}

function attachDependenciesProp(proto) {
	['dep', 'req', 'requirements'].forEach(function(propName) {
		Object.defineProperty(proto, propName, {
			get: function() {
				return [].concat(this._requirements);
			},

			set: function(value) {
	        	this._requirements = parseRequirements(value);
	    	}
		});
	});
}
/* Recipe definition */

function Recipe(opts) {
    // if opts is a string, then map this to opts.text
    if (typeof opts == 'string' || (opts instanceof String)) {
        opts = {
            text: opts
        };
    }

    // ensure the opts have been initialised
    opts = opts || {};

    // define the sections
    this.sections = opts.sections || {};

    // define the versions
    this._versions = ['latest'];
    
    // initialise requirements
    this._requirements = [].concat(opts.requirements || []);

    // if we have text, then parse the text
    if (opts.text) {
        this.parse(opts.text, opts);
    }
    // otherwise map the contents of the opts across to the recipe
    else {
        for (var key in opts) {
            // if the key is in the opts
            if (opts.hasOwnProperty(key) && typeof this[key] != 'function') {
                this[key] = opts[key];
            }
        }
    }
}

Recipe.prototype = {
    parse: function(text, opts) {
        var match, downloadMatch,
            activeSection,
            lines = (text || '').split(/\n/),
            line,
            inSections = false,
            ii, count;
            
        // iterate through the lines until we hit the first section
        for (ii = 0; ii < lines.length; ii++) {
            // get a reference to the line
            line = lines[ii];
            
            // check to see if we are in the sections
            inSections = inSections || reSection.test(line) || reDownload.test(line);
            
            // if we are not in the sections, then check for field matches
            if (! inSections) {
                match = reField.exec(line);
                if (match) {
                    this[match[1]] = match[2];
                }
            }
            else {
                match = reSection.exec(line);
                if (match) {
                    // create the active section
                    activeSection = this.sections[match[1]] = new RecipeSection(this, match[1]);
                }
                else {
                    // if we don't have an active section, then create the core section
                    if (! activeSection) {
                        activeSection = this.sections.core = new RecipeSection(this, 'core');
                    }

                    activeSection.parse(line, opts);
                }
            }
        }
    }
};

// create a dsc alias for desc
Object.defineProperty(Recipe.prototype, 'dsc', {
    set: function(value) {
        this.desc = value;
    }
});

// create a "see" alias for redirect
Object.defineProperty(Recipe.prototype, 'see', {
    set: function(value) {
        this.redirect = value;
    }
});

// create the versions parser
Object.defineProperty(Recipe.prototype, 'versions', {
    get: function() {
        return this._versions;
    },

    set: function(value) {
        this._versions = parseVersions(value);
    }
});


// define the depdency property parsers
attachDependenciesProp(Recipe.prototype);
/**
# RecipeSection

A RecipeSection is used to define a part of the recipe.  In the case of a simple
library, it is likely that only a core section is defined, however, more complicated
recipes (e.g. Twitter Bootstrap) will define sections that contain SectionVariants.
*/
function RecipeSection(recipe, name) {
    this.recipe = recipe;
    this.name = name;
    this.targets = {};
    this._variants = [];
    this._requirements = [];
}

RecipeSection.prototype = {
    parse: function(line, opts) {
        // check for a download match
        var downloadMatch = reDownload.exec(line);

        // if we have a download match, then add it to the active section
        if (downloadMatch) {
            var fileType = downloadMatch[1] || 'js';

            // if the file type is not in the active section then add it
            if (! this.targets[fileType]) {
                this.targets[fileType] = [];
            }

            // add the download to the file types in the active section
            this.targets[fileType].push(formatter(downloadMatch[2])(this.recipe));
        }
        else {
            var fieldMatch = reField.exec(line);

            // if this was a field match, then apply the field
            if (fieldMatch) {
                this[fieldMatch[1]] = fieldMatch[2];
            }
        }
    },

    toJSON: function() {
        return {
            targets: this.targets,
            variants: this._variants
        };
    }
};

Object.defineProperty(RecipeSection.prototype, 'variants', {
    get: function() {
        return [].concat(this._variants);
    },

    set: function(value) {
        var section = this;

        if (typeof value == 'string' || (value instanceof String)) {
            value.split(reCommaDelim).forEach(function(text) {
                section._variants.push(new SectionVariant(text));
            });
        }
        else {
            this._variants = value;
        }
    }
});

// attach a dependencies prop
attachDependenciesProp(RecipeSection.prototype);
/**
# SectionVariant

A SectionVariant is a type that represents a variation of the current
section.  Section variants are used when a file (or set of files) for
a number of optional extras for a recipe follows a sensible pattern.

It should be noted that it is not required to define all variants in 
the recipe, as the information provided in the recipe is simply used 
to determine variant dependencies and also provide metadata to tools 
that consume recipes about the variants of the module that are 
available.
*/
function SectionVariant(text) {
	var complexMatch = reComplexVariant.exec(text);

	// if we have a complex match the cleanup
	if (complexMatch) {
		console.log(complexMatch);
	}
	else {
		this.name = text;
	}
}

SectionVariant.prototype = {

};

Recipe.prototype.resolve = function(callback) {
    
};

var recipe = {
	define: function(opts) {
		return new Recipe(opts);
	}
};

if (typeof recipe != 'undefined') {
    module.exports = recipe;
}
