function parseRequirements(text) {
	var requirements = text.split(reCommaDelim);

	// iterate through the requirements and update to absolute paths if it they are local
	requirements.forEach(function(req, index) {
	    if (reLocalFile.test(req)) {
	        requirements[index] = path.resolve(recipe.basePath, req);
	    }
	});

	return requirements;
}

function parseVersions(value) {
    if (typeof value == 'string' || (value instanceof String)) {
        value = value.split(reCommaDelim);

        // if we don't have a latest version specified, then add that to the list
        if (value.indexOf('latest') < 0) {
            value[value.length] = 'latest';
        }
    }

    return value;
}

function attachDependenciesProp(proto) {
	['dep', 'req', 'requirements'].forEach(function(propName) {
		Object.defineProperty(proto, propName, {
			get: function() {
				return [].concat(this._requirements);
			},

			set: function(value) {
	        	this._requirements = parseRequirements(value);
	    	}
		});
	});
}