/* Recipe definition */

function Recipe(opts) {
    // if opts is a string, then map this to opts.text
    if (typeof opts == 'string' || (opts instanceof String)) {
        opts = {
            text: opts
        };
    }

    // ensure the opts have been initialised
    opts = opts || {};

    // define the sections
    this.sections = opts.sections || {};

    // define the versions
    this._versions = ['latest'];
    
    // initialise requirements
    this._requirements = [].concat(opts.requirements || []);

    // if we have text, then parse the text
    if (opts.text) {
        this.parse(opts.text, opts);
    }
    // otherwise map the contents of the opts across to the recipe
    else {
        for (var key in opts) {
            // if the key is in the opts
            if (opts.hasOwnProperty(key) && typeof this[key] != 'function') {
                this[key] = opts[key];
            }
        }
    }
}

Recipe.prototype = {
    parse: function(text, opts) {
        var match, downloadMatch,
            activeSection,
            lines = (text || '').split(/\n/),
            line,
            inSections = false,
            ii, count;
            
        // iterate through the lines until we hit the first section
        for (ii = 0; ii < lines.length; ii++) {
            // get a reference to the line
            line = lines[ii];
            
            // check to see if we are in the sections
            inSections = inSections || reSection.test(line) || reDownload.test(line);
            
            // if we are not in the sections, then check for field matches
            if (! inSections) {
                match = reField.exec(line);
                if (match) {
                    this[match[1]] = match[2];
                }
            }
            else {
                match = reSection.exec(line);
                if (match) {
                    // create the active section
                    activeSection = this.sections[match[1]] = new RecipeSection(this, match[1]);
                }
                else {
                    // if we don't have an active section, then create the core section
                    if (! activeSection) {
                        activeSection = this.sections.core = new RecipeSection(this, 'core');
                    }

                    activeSection.parse(line, opts);
                }
            }
        }
    }
};

// create a dsc alias for desc
Object.defineProperty(Recipe.prototype, 'dsc', {
    set: function(value) {
        this.desc = value;
    }
});

// create a "see" alias for redirect
Object.defineProperty(Recipe.prototype, 'see', {
    set: function(value) {
        this.redirect = value;
    }
});

// create the versions parser
Object.defineProperty(Recipe.prototype, 'versions', {
    get: function() {
        return this._versions;
    },

    set: function(value) {
        this._versions = parseVersions(value);
    }
});


// define the depdency property parsers
attachDependenciesProp(Recipe.prototype);