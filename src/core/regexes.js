var reDownload = /^([\w\.\-]*?)\s*<\=\s*(.*)$/,
    reCommaDelim = /\,\s*/,
    reField = /^\s*\#?\s*(\w+)\:\s*(.*)$/,
    reLocalFile = /^[\/\\\.]/,
    reSection = /\s*\[(\w+)\]/,
    reComplexVariant = /^(\w+)\s*\[(.*)\]\s*$/;