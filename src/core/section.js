/**
# RecipeSection

A RecipeSection is used to define a part of the recipe.  In the case of a simple
library, it is likely that only a core section is defined, however, more complicated
recipes (e.g. Twitter Bootstrap) will define sections that contain SectionVariants.
*/
function RecipeSection(recipe, name) {
    this.recipe = recipe;
    this.name = name;
    this.targets = {};
    this._variants = [];
    this._requirements = [];
}

RecipeSection.prototype = {
    parse: function(line, opts) {
        // check for a download match
        var downloadMatch = reDownload.exec(line);

        // if we have a download match, then add it to the active section
        if (downloadMatch) {
            var fileType = downloadMatch[1] || 'js';

            // if the file type is not in the active section then add it
            if (! this.targets[fileType]) {
                this.targets[fileType] = [];
            }

            // add the download to the file types in the active section
            this.targets[fileType].push(formatter(downloadMatch[2])(this.recipe));
        }
        else {
            var fieldMatch = reField.exec(line);

            // if this was a field match, then apply the field
            if (fieldMatch) {
                this[fieldMatch[1]] = fieldMatch[2];
            }
        }
    },

    toJSON: function() {
        return {
            targets: this.targets,
            variants: this._variants
        };
    }
};

Object.defineProperty(RecipeSection.prototype, 'variants', {
    get: function() {
        return [].concat(this._variants);
    },

    set: function(value) {
        var section = this;

        if (typeof value == 'string' || (value instanceof String)) {
            value.split(reCommaDelim).forEach(function(text) {
                section._variants.push(new SectionVariant(text));
            });
        }
        else {
            this._variants = value;
        }
    }
});

// attach a dependencies prop
attachDependenciesProp(RecipeSection.prototype);