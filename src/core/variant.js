/**
# SectionVariant

A SectionVariant is a type that represents a variation of the current
section.  Section variants are used when a file (or set of files) for
a number of optional extras for a recipe follows a sensible pattern.

It should be noted that it is not required to define all variants in 
the recipe, as the information provided in the recipe is simply used 
to determine variant dependencies and also provide metadata to tools 
that consume recipes about the variants of the module that are 
available.
*/
function SectionVariant(text) {
	var complexMatch = reComplexVariant.exec(text);

	// if we have a complex match the cleanup
	if (complexMatch) {
		console.log(complexMatch);
	}
	else {
		this.name = text;
	}
}

SectionVariant.prototype = {

};