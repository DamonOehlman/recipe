// req: formatter

//= core/regexes
//= core/parsers
//= core/recipe
//= core/section
//= core/variant

//= core/{{ packageType }}/recipe-ext  : core/recipe-noext

var recipe = {
	define: function(opts) {
		return new Recipe(opts);
	}
};