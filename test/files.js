var recipe = require('../dist/commonjs/recipe'),
    assert = require('assert'),
    fs = require('fs'),
    path = require('path'),
    recipePath = path.resolve(__dirname, 'recipes');

describe('file based recipe tests', function() {
    var files = fs.readdirSync(recipePath) || [];

    files.forEach(function(file) {
        var recipeText = fs.readFileSync(path.join(recipePath, file), 'utf8');

        describe('validate "' + file + '" recipe', function() {
            it('should have recipe text to parse', function() {
                assert(recipeText);
            });

            require('./recipe-validators/' + path.basename(file, '.recipe'))(recipe.define(recipeText));
        });
    });
});