var recipe = require('../dist/commonjs/recipe'),
    expect = require('expect.js');

describe('recipe initialization from opts tests', function() {
    it('should be able to initialize a recipe with just a description', function() {
        var test = recipe.define({ desc: 'A test recipe' });

        expect(test).to.be.ok();
        expect(test.desc).to.equal('A test recipe');
    });

    it('should be able to initialize a recipe with a see redirect', function() {
        var test = recipe.define({ see: 'DamonOehlman/classtweak' });

        expect(test).to.be.ok();
        expect(test.redirect).to.equal('DamonOehlman/classtweak');
    });
});