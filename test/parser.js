var recipe = require('../dist/commonjs/recipe'),
    expect = require('expect.js'),
    sniff = require('sniff'),
    samples = {
        'simple':
        '# dsc: small JS library for DOM class manipulation\n' +
        '# url: https://github.com/DamonOehlman/classtweak\n' +
        '# bug: https://github.com/DamonOehlman/classtweak/issues\n' +
        '# dep: jquery\n\n' +
        '[core]\n' +
        'js <= github://DamonOehlman/classtweak/classtweak.js',

        'alternative format':
        '# dsc: Another test library\n' +
        '# url: http://test.com/\n' +
        '# req: jquery\n\n' +
        '[core]\n' +
        'js <= github://DamonOehlman/classtweak/classtweak.js',

        'no section declaration':
        '# dsc: No Section Test\n' +
        '# url: http://test.com/\n' +
        '# req: jquery\n\n' +
        'js <= github://DamonOehlman/classtweak/classtweak.js',

        'comment-less':
        'dsc: Commentless recipe\n' +
        'url: http://test.com/\n' +
        'req: jquery\n\n' +
        'js <= github://DamonOehlman/classtweak/classtweak.js',

        'leading-spaces':
        ' dsc: Leading Spaces Recipe\n' +
        ' url: http://test.com/\n' +
        '      req: jquery\n\n' +
        'js <= github://DamonOehlman/classtweak/classtweak.js',

        'no-metadata':
        'js <= github://DamonOehlman/classtweak/classtweak.js'
    };

function checkSample(sampleName) {
    var testRecipe = recipe.define(samples[sampleName]);

    describe('(' + sampleName + ') metadata', function() {
        it('should have a description', function() {
            expect(testRecipe.desc).to.be.ok();
        });

        it('should have a url', function() {
            expect(testRecipe.url).to.be.ok();
        });

        it('should require jquery', function() {
            expect(testRecipe.requirements.length).to.be.above(0);
            expect(testRecipe.requirements[0]).to.equal('jquery');
        });
    });

    checkSampleSection(sampleName);
}

function checkSampleSection(sampleName) {
    var testRecipe = recipe.define(samples[sampleName]);

    describe('(' + sampleName + ') core section', function() {
        it('should have a core section', function() {
            expect(testRecipe.sections.core).to.be.ok();
        });

        it('should have data for the core section', function() {
            var core = testRecipe.sections.core;
            
            expect(core.targets.js).to.be.ok();
            expect(sniff(core.targets.js)).to.equal('array');
            expect(core.targets.js.length).to.equal(1);
            expect(core.targets.js[0]).to.equal('github://DamonOehlman/classtweak/classtweak.js');
        });
    });
}

describe('recipe parser tests', function() {
    checkSample('simple');
    checkSample('alternative format');
    checkSample('no section declaration');
    checkSample('comment-less');
    checkSample('leading-spaces');

    checkSampleSection('no-metadata');
});