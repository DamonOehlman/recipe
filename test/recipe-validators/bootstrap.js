var assert = require('assert'),
	sniff = require('sniff'),
	_ = require('underscore');

module.exports = function(recipe) {
	// execute the core tests
	require('./core')(recipe, {
		desc: 'HTML, CSS, and JS toolkit from Twitter',
		author: 'twitter',
		requirements: ['jquery']
	});

	it('should have an addin section', function() {
		assert(recipe.sections.addin, 'Unable to find addin section');
	});

	it('should have variants defined', function() {
		assert(recipe.sections.addin.variants.length > 0);
	});

	it('should be able to locate the dropdown variant', function() {
		var dropdowns = recipe.sections.addin.variants.filter(function(variant) {
			return variant.name === 'dropdown';
		});

		assert(dropdowns.length > 0, 'Unable to locate expected variant');
	});

	it('addin section should include the correct url', function() {
		assert.equal(sniff(recipe.sections.addin.targets.js), 'array');
	});
};