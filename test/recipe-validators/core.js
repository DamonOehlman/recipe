var assert = require('assert');

module.exports = function(recipe, details) {
	// ensure the details have been defined
	details = details || {};

	// check the description
	it('should have a valid description', function() {
		if (typeof details.desc != 'undefined') {
			assert.equal(recipe.desc, details.desc);
		}
	});

	// check the author
	it('should have a valid author', function() {
		if (typeof details.author != 'undefined') {
			assert.equal(recipe.author, details.author);
		}
	});

	// check the url
	if (typeof details.url != 'undefined') {
		it('should have a valid url', function() {
			assert.equal(recipe.url, details.url);
		});
	}

	// check the requirements
	if (typeof details.requirements != 'undefined') {
		it('should have specific requirements', function() {
			assert.deepEqual(recipe.requirements, details.requirements);
		});
	}

	// console.log(JSON.stringify(recipe, false, 2));
};