var recipe = require('../dist/commonjs/recipe'),
    expect = require('expect.js'),
    sniff = require('sniff');

describe('section dependency recipe checks', function() {
    var recipeText =
        'dsc: Section Deps Test\n' +
        '[core]\n' +
        'js <= github://DamonOehlman/classtweak/classtweak.js\n' +
        '[awesome]\n' +
        'dep: jquery\n' +
        'js <= github://DamonOehlman/matchme/dist/{{ packageType }}/matchme.js',
    testRecipe = recipe.define(recipeText);


    it('should have a description', function() {
        expect(testRecipe.desc).to.equal('Section Deps Test');
    });

    it('should have an awesome section', function() {
        expect(testRecipe.sections.awesome).to.be.ok();
    });

    it('find a dependency of jquery for the awesome section', function() {
        var testSection = testRecipe.sections.awesome;

        expect(sniff(testSection.requirements)).to.equal('array');
        expect(testSection.requirements.length).to.be.above(0);
        expect(testSection.requirements[0]).to.equal('jquery');
    });
});