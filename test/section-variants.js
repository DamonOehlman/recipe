var recipe = require('../dist/commonjs/recipe'),
    expect = require('expect.js'),
    sniff = require('sniff');

describe('section dependency recipe checks', function() {
    var recipeText =
            'dsc: Section Variants Test\n' +
            '[core]\n' +
            'js <= github://DamonOehlman/classtweak/classtweak.js\n' +
            '[addin]\n' +
            'variants: dropdown, modal\n' +
            'js <= github://twitter/bootstrap/js/bootstrap-$addin.js',
    testRecipe = recipe.define(recipeText);


    it('should have a description', function() {
        expect(testRecipe.desc).to.equal('Section Variants Test');
    });

    it('should have an addin section', function() {
        expect(testRecipe.sections.addin).to.be.ok();
    });

    it('find variants for the addin section', function() {
        var testSection = testRecipe.sections.addin;

        expect(sniff(testSection.requirements)).to.equal('array');
        expect(testSection.requirements.length).to.equal(0);
        expect(testSection.variants.length).to.equal(2);
        expect(testSection.variants[0].name).to.equal('dropdown');
    });
});