var recipe = require('../dist/commonjs/recipe'),
    expect = require('expect.js'),
    sniff = require('sniff');

describe('version tests', function() {

    describe('default version information', function() {
        var recipeText =
                'dsc: Version Test\n' +
                '[core]\n' +
                'js <= github://DamonOehlman/matchme/classtweak.js\n',
            testRecipe = recipe.define(recipeText);


        it('should have a description', function() {
            expect(testRecipe.desc).to.equal('Version Test');
        });

        it('should have only one version specified (latest)', function() {
            expect(sniff(testRecipe.versions)).to.equal('array');
            expect(testRecipe.versions.length).to.equal(1);
            expect(testRecipe.versions).to.contain('latest');
        });
    });

    describe('only one version specified', function() {
        var recipeText =
                'dsc: Version Test\n' +
                'versions: 0.1\n' +
                '[core]\n' +
                'js <= github://DamonOehlman/matchme/classtweak.js\n',
            testRecipe = recipe.define(recipeText);


        it('should have a description', function() {
            expect(testRecipe.desc).to.equal('Version Test');
        });

        it('should have only one version specified (latest)', function() {
            expect(sniff(testRecipe.versions)).to.equal('array');
            expect(testRecipe.versions.length).to.equal(2);
            expect(testRecipe.versions).to.contain('latest');
            expect(testRecipe.versions).to.contain('0.1');
        });
    });

    describe('multiple versions specified', function() {
        var recipeText =
                'dsc: Version Test\n' +
                'versions: 0.1, 0.2\n' +
                '[core]\n' +
                'js <= github://DamonOehlman/matchme/classtweak.js\n',
            testRecipe = recipe.define(recipeText);


        it('should have a description', function() {
            expect(testRecipe.desc).to.equal('Version Test');
        });

        it('should have only one version specified (latest)', function() {
            expect(sniff(testRecipe.versions)).to.equal('array');
            expect(testRecipe.versions.length).to.equal(3);
            expect(testRecipe.versions).to.contain('latest');
            expect(testRecipe.versions).to.contain('0.1');
            expect(testRecipe.versions).to.contain('0.2');
        });
    });
});